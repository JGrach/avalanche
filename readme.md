# Jour Blanc (projet avalanche)

Programme Arduino pour l'artiste [Fanny Papot](https://www.instagram.com/fannypapot/) et son oeuvre Jour Blanc.

![Jour Blanc](./jour_blanc.jpg)

Le programme simule, grâce à 8 rubans de 17 led chacuns, le cours de la lumière du jour sur la silhouette enneigée de montagnes fantomatiques.
 
Comme pour raconter notre incapacité à maîtriser le monde même lorsqu'il est mis en boîte, le parcours du soleil artificiel est recalculé aléatoirement à chaque passage. Le jeu d'ombre qu'il provoque est imprévisible.

## Contraintes

L'artiste savait intégrer des progammes arduino sur son hardware et avait une connaissance théorique de la programmation quoi qu'insuffisante pour réaliser elle même le programme. Néanmoins, elle souhaitait pouvoir moduler les valeurs de randomization de la course ou les effets de lumières d'elle même, une fois dans sa phase d'installation. J'ai générisé le maximum de valeurs que je pouvais dans le temps imparti du développement (2 jours) et j'ai positionné les constantes d'initialisation au début du programme dans un soucis de lui rendre évident la localisation des valeurs intéressantes pour elle.