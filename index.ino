#include <Arduino.h> // maybe you add to delete this line
#include <Adafruit_NeoPixel.h>

/*-----------------------------------------------
TIME HANDLING - touch to change the speed
-----------------------------------------------*/

#define NUMBER_OF_LED_BRIGHTENING 5

#define SPEED_OF_INTENSITY_INCREMENT 2 // MAXIMUM = 255
#define SPEED_OF_INTENSITY_DECREMENT 2 // MAXIMUM = 255
#define LOOP_DELAY_IN_MILLIS 1

#define MAXIMAL_BRIGTHENING 255 // MAXIMUM = 255

/*-----------------------------------------------
MOVEMENT HANDLING - touch to change how is the light movement
-----------------------------------------------*/

#define VARIATION_FROM_THE_LAST_LED_ID 4  // constraint of the horizontal movement
#define COLUMNS_BORDER_CONSTRAINT 2       // constraint the border (how many columns from the border will never turning on)
#define FIRST_LED_ON (COLUMNS_OF_LED / 2) // begin at a variadic center

/*-----------------------------------------------
SETUP MANAGEMENT
-----------------------------------------------*/

#define REAL_MAXIMAL_BRIGTHENING (MAXIMAL_BRIGTHENING - (MAXIMAL_BRIGTHENING % SPEED_OF_INTENSITY_INCREMENT))

#define LED_PIN 6

#define LINES_OF_LED 8
#define COLUMNS_OF_LED 17
#define TOTAL_LED (LINES_OF_LED * COLUMNS_OF_LED)

int leds[LINES_OF_LED][COLUMNS_OF_LED];                                                 // coordinates table
int focusLedId;                                                                         // current led with focus
Adafruit_NeoPixel strip = Adafruit_NeoPixel(TOTAL_LED, LED_PIN, NEO_GRBW + NEO_KHZ800); // led library assignment

unsigned long lastLoopTime = 0; // use to chronometer

/*-----------------------------------------------
COORDINATES FUNCTIONS
-----------------------------------------------*/

int ledIdToX(int ledId)
{
  return ledId % COLUMNS_OF_LED;
}

int ledIdToY(int ledId)
{
  return ledId / COLUMNS_OF_LED;
}

int computeRandomX(int ledIdConstraint)
{
  const int minBorderX = 0 + COLUMNS_BORDER_CONSTRAINT + (NUMBER_OF_LED_BRIGHTENING / 2);
  const int maxBorderX = COLUMNS_OF_LED - COLUMNS_BORDER_CONSTRAINT - (NUMBER_OF_LED_BRIGHTENING / 2) + ((NUMBER_OF_LED_BRIGHTENING % 2) - 1);

  int minCandidate = ledIdToX(ledIdConstraint) - VARIATION_FROM_THE_LAST_LED_ID;
  int maxCandidate = ledIdToX(ledIdConstraint) + VARIATION_FROM_THE_LAST_LED_ID;

  int min = minCandidate < minBorderX ? minBorderX : minCandidate;
  int max = maxCandidate >= maxBorderX ? maxBorderX : maxCandidate;

  return random(min, max);
}

int nextLedId()
{
  int y = ledIdToY(focusLedId) + 1;
  int x = ledIdToX(focusLedId);

  return leds[y >= LINES_OF_LED ? 0 : y][computeRandomX(x)];
}

/*-----------------------------------------------
BRIGHTENING FUNCTIONS
-----------------------------------------------*/

uint8_t getBrightness(int ledId)
{
  int byte = 8;
  int colorNumbers = 3;
  return strip.getPixelColor(ledId) >> byte * colorNumbers;
}

void updateBrightness()
{
  int leftLedId = focusLedId - (NUMBER_OF_LED_BRIGHTENING / 2);
  int rightLedId = focusLedId + (NUMBER_OF_LED_BRIGHTENING / 2) + ((NUMBER_OF_LED_BRIGHTENING % 2) - 1);

  for (int ledId = 0; ledId < TOTAL_LED; ledId++)
  {
    uint8_t brightness = getBrightness(ledId);
    if (ledId >= leftLedId && ledId <= rightLedId && brightness < REAL_MAXIMAL_BRIGTHENING)
      strip.setPixelColor(ledId, strip.Color(0, 0, 0, brightness + SPEED_OF_INTENSITY_INCREMENT));
    else if (brightness != 0)
      strip.setPixelColor(ledId, strip.Color(0, 0, 0, brightness - SPEED_OF_INTENSITY_DECREMENT));
  }
}

/*-----------------------------------------------
ARDUINO FUNCTIONS
-----------------------------------------------*/

void setup()
{
  strip.begin();

  for (int ledId = 0; ledId < TOTAL_LED; ledId++)
  {
    int y = ledId / COLUMNS_OF_LED;
    int x = ledId % COLUMNS_OF_LED;

    leds[y][x] = ledId;
  }

  focusLedId = leds[0][computeRandomX(FIRST_LED_ON)];
}

void loop()
{
  if (millis() - lastLoopTime > LOOP_DELAY_IN_MILLIS)
  {
    updateBrightness();
    strip.show();

    if (getBrightness(focusLedId) >= REAL_MAXIMAL_BRIGTHENING)
    {
      focusLedId = nextLedId();
    }

    lastLoopTime = millis();
  }
}
